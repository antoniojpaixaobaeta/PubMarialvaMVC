﻿using PubMarialvaMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PubMarialvaMVC.Controllers
{

    public class HomeController : Controller
    {
        DataClasses1DataContext db = new DataClasses1DataContext();
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Menu()
        {
            var hoje = DateTime.Now;
            var amanha = hoje.AddDays(1);
            var depois = hoje.AddDays(2);

            List<Menus> menu = db.Menus.Where(men => men.DataDoMenu == hoje).ToList();
            
            return View(menu);

        }
        

        public ActionResult Local()
        {
            return View();
        }

        public ActionResult Reserva()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Reserva(Reserva reserva)
        {
            if (ModelState.IsValid)
            {
                db.Reservas.InsertOnSubmit(reserva);
            }

            try
            {
                db.SubmitChanges();

                return RedirectToAction("Sucesso", "Home");
            }
            catch
            {
                return RedirectToAction("Error", "Home");
            }
        }
        public ActionResult Error()
        {
            return View();
        }
        public ActionResult Sucesso()
        {
            return View();
        }
    }
}